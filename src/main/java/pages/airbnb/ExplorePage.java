package pages.airbnb;

import core.BasePage;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

import java.lang.reflect.Array;
import java.util.List;

public class ExplorePage extends BasePage {

    //Constructor
    public ExplorePage(AndroidDriver driver) {
        super(driver);
    }

    //Locators
    //Where are you going text
    @AndroidFindBy(accessibility = "Where are you going? Navigate to start your search.")
    private AndroidElement whereText;

    //Destination field
    @AndroidFindBy (uiAutomator = "new UiSelector().text(\"Where are you going?Navigate forward to access search suggestions.\")")
    private AndroidElement destinationTxBx;

    //NotSure text field
    @AndroidFindBy(id = "com.airbnb.android:id/text")
    private AndroidElement notSureTx;

    //Destination results
    @AndroidFindBy(id = "com.airbnb.android:id/title")
    private AndroidElement destinationResults;

    //Back arrow icon
    @AndroidFindBy(accessibility = "Navigate Up")
    private AndroidElement backArrow;

    //Methods

    /**
     * Check if Explore page is present
     * @return
     */
    public boolean isExplorePagePresent(){
        System.out.println("Trying to check if Explore page is present...");
        return notSureTx.isDisplayed();
    }


    //Click Where are you going method
    public void whereAreYouGoingClick () {
        System.out.println("Trying to click on Where are you going method...");
        whereText.click();
        System.out.println("Where are you going clicked!");
    }

    /**
     * Enter destination Method
     * @param destination
     */
    public void enterDestination (String destination) {
        System.out.println("Trying to enter destination field...");
        destinationTxBx.sendKeys(destination);
        System.out.println("Destination entered");
    }

    /**
     * click on the final destination
     * @param finalDestination
     */
    public void findDestinationClick (String finalDestination) throws InterruptedException {
        System.out.println("Trying to click final destination...");
        waitForElementToBeClickable(By.id("com.airbnb.android:id/title"));
        List<AndroidElement> elements = driver.findElementsById("com.airbnb.android:id/title");
        System.out.println("List size: "+elements.size());
        //For to see elements from List
        System.out.println("Values found:");
        for (AndroidElement element:elements) {
            System.out.println(element.getText());
        }
        for (int i =0; i<elements.size();i++){
            if (elements.get(i).getText().equalsIgnoreCase(finalDestination)){
                System.out.println("City was found...");
                elements.get(i).click();
                break;
            }else
                System.out.println("Current value is not matching: "+elements.get(i).getText());
        }
    }

    /**
     * Back arrow click
     */
    public void clickBackArrow(){
        System.out.println("Trying to click back arrow...");
        backArrow.click();
        System.out.println("Back arrow was clicked!");
    }

    /**
     * Look for a destination method
     * @param destination
     */
    public void lookForDestination(String destination){
        whereAreYouGoingClick();
        enterDestination(destination);
        hideKeyboard();
    }
}
