package pages.selendroid;

import core.BasePage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class HomePage extends BasePage {

    //Constructor
    public HomePage(AndroidDriver driver) {
        super(driver);
    }

    //Locator
    @AndroidFindBy(id="android:id/button1")
    private AndroidElement dismissBtn;

    @AndroidFindBy(id="android:id/alertTitle")
    private AndroidElement alertMsg;

    //Methods

    /**
     * Dismiss pop-up
     */
    public void clickDismissBtn(){
        System.out.println("Trying to click Dismiss button...");
        dismissBtn.click();
        System.out.println("PopUp Dismissed");
    }

    /**
     * Verify pop-up
     * @return True if Alert is present
     * @throws InterruptedException
     */
    public boolean verifyPopUp() throws InterruptedException {
        System.out.println("Validating if alert is present...");
        Thread.sleep(2000);
        return alertMsg.isDisplayed();
    }

}
