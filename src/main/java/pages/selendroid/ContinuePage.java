package pages.selendroid;

import core.BasePage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class ContinuePage extends BasePage {

    //Constructor
    public ContinuePage(AndroidDriver driver) {
        super(driver);
    }

    //Locator
    @AndroidFindBy(id="com.android.permissioncontroller:id/continue_button")
    private AndroidElement continueBtn;

    //Methods
    /**
     * Click on continue button
     */
    public void clickContinueBtn(){
        System.out.println("Trying to click continue button...");
        continueBtn.click();
        System.out.println("Continue button clicked!");
    }
}

