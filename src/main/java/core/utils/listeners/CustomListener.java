package core.utils.listeners;

import core.BasePage;
import core.BaseTest;
import io.appium.java_client.android.AndroidDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.IOException;

public class CustomListener implements ITestListener {

    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("----------------------------------");
        System.out.println("Start of execution(TEST)-> "+result.getName());
        System.out.println("----------------------------------");
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("----------------------------------");
        System.out.println("Test Pass-> "+result.getName());
        System.out.println("----------------------------------");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println("----------------------------------");
        System.out.println("Test Failed-> "+result.getName());
        System.out.println("----------------------------------");
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.println("----------------------------------");
        System.out.println("Test Skipped-> "+result.getName());
        System.out.println("----------------------------------");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {
        System.out.println("----------------------------------");
        System.out.println("Start of Execution-> "+context.getName());
        System.out.println("----------------------------------");
    }

    @Override
    public void onFinish(ITestContext context) {
        System.out.println("----------------------------------");
        System.out.println("End of Execution-> "+context.getName());
        System.out.println("----------------------------------");
    }
}
