package selendroid;

import core.BaseTest;
import core.BaseTestBrowserStack;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.selendroid.ContinuePage;
import pages.selendroid.HomePage;

public class TestSelendroid extends BaseTestBrowserStack {

    @Test
    public void testSelendroidApp() throws InterruptedException {

        ContinuePage continueObj = new ContinuePage(getDriver());
        HomePage homeObj = new HomePage(getDriver());

        //Code
        continueObj.clickContinueBtn();

        //Validate alert is present
        Assert.assertTrue(homeObj.verifyPopUp(),"Alert is not present");

        Thread.sleep(5000);

        //Dismiss alert
        homeObj.clickDismissBtn();

    }
}
