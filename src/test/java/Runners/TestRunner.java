package Runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/test/resources/features/Profile.feature",
        glue = {"StepsDefinitions"}
)

public class TestRunner extends AbstractTestNGCucumberTests {
}
