package airbnb;

import core.BaseTest;
import io.appium.java_client.imagecomparison.FeatureDetector;
import io.appium.java_client.imagecomparison.FeaturesMatchingOptions;
import io.appium.java_client.imagecomparison.FeaturesMatchingResult;
import io.appium.java_client.imagecomparison.MatchingFunction;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.OutputType;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.airbnb.BarraPage;
import pages.airbnb.ExplorePage;
import pages.airbnb.LoginPage;
import pages.airbnb.ProfilePage;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class DestinationsTest extends BaseTest {

    //Class objects
    ExplorePage exploreObj;

    @Test(priority = 1, groups = "destinations")
    public void testDestinationPlaya() throws InterruptedException, IOException {

        //Class objects
        ExplorePage exploreObj = new ExplorePage(getDriver());

        //Code
        //Verify explore page
        Assert.assertTrue(exploreObj.isExplorePagePresent(),"Explore page is NOT displayed");

        //exploreObj.takeScreenShot("src/main/resources/airbnb/ss/","findByImage");

        driver.findElementsByImage("src/main/resources/airbnb/ss/findByImage.jpg");






        //Look for destination Playa using a JSON file
        //exploreObj.lookForDestination(exploreObj.getValueJSON("src/main/resources/airbnb/destination.json","Playa"));
        //exploreObj.clickBackArrow();
    }
    /*
    @Test(priority = 2, groups = "destinations")
    public void testDestinationCatarata() throws InterruptedException {

        //Class objects
        exploreObj = new ExplorePage(getDriver());

        //Verify explore page
        Assert.assertTrue(exploreObj.isExplorePagePresent(),"Explore page is NOT displayed");

        //Look for destination Catarata using excel file
        exploreObj.lookForDestination(exploreObj.getValueFromExcel("src/main/resources/airbnb/destinations.xlsx","Cataratas"));
        exploreObj.clickBackArrow();
    }

    @Test(priority = 3, groups = "template")
    public void testDestinationRestaurante() throws InterruptedException {

        //Class objects
        exploreObj = new ExplorePage(getDriver());

        //Verify explore page
        Assert.assertTrue(exploreObj.isExplorePagePresent(),"Explore page is NOT displayed");

        //Find a Restaurant using JSON file
        exploreObj.lookForDestination(exploreObj.getValueJSON("src/main/resources/airbnb/destination.json","Restaurante"));
        exploreObj.clickBackArrow();
    }

    @Test(priority = 4, groups = "template")
    public void testDestinationCancun() throws InterruptedException {

        //Class objects
        exploreObj = new ExplorePage(getDriver());

        //Verify explore page
        Assert.assertTrue(exploreObj.isExplorePagePresent(),"Explore page is NOT displayed");

        //Select a value from result search based on a text (Cancún)
        exploreObj.lookForDestination(exploreObj.getValueJSON("src/main/resources/airbnb/destination.json","Playa"));
        exploreObj.findDestinationClick("Cancun, Cancún");
        exploreObj.clickBackArrow();
        exploreObj.clickBackArrow();
    }
     */
}
