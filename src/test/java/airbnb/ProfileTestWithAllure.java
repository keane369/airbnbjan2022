package airbnb;

import core.BaseTest;
import core.utils.listeners.AllureListener;
import core.utils.listeners.CustomListener;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.airbnb.BarraPage;
import pages.airbnb.ProfilePage;

import java.io.IOException;
import java.lang.reflect.Method;

import static core.utils.extentreports.ExtentTestManager.startTest;

@Listeners(AllureListener.class)
public class ProfileTestWithAllure extends BaseTest {

    ProfilePage profileObj;
    String pathSS = "src/main/resources/airbnb/ss/";

    @Test(priority = 4, groups = {"profile","smoke"})
    @Feature("AirBnB Smoke Test")
    @Story("Smoke Test")
    @Description("Verifies that the main features of Airbnb are working")
    public void testProfile(Method method) throws IOException {
        startTest(method.getName(),"This is to get profile Page");
        //Class objects
        BarraPage barraObj = new BarraPage(getDriver());
        profileObj = new ProfilePage(getDriver());
        //Click on Profile
        barraObj.profileButtonClick();
        //Verify Profile page
        Assert.assertTrue(profileObj.verifyProfileText());
    }
/*
    @Test(priority = 5, groups = {"profile","smoke"})
    public void testHosting(Method method) throws IOException {
        startTest(method.getName(),"Validating Hosting field");
        //Scroll
        profileObj.scrollToText("Learn about hosting");
        //profileObj.takeScreenShot(pathSS, "profile_1");
    }

    @Test(priority = 5, groups = "profile")
    public void testMoney(Method method) throws IOException {
        startTest(method.getName(),"Validating Money Field");
        //Scroll to partial text
        profileObj.scrollToPartialText("money");
        //Screenshot of Profile bottom page
        //profileObj.takeScreenShot(pathSS, "profile_2");
        Assert.assertTrue(false);
    }

 */
}