package airbnb;
import core.BaseTest;
import core.utils.listeners.CustomListener;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.airbnb.BarraPage;
import pages.airbnb.ProfilePage;
import java.io.IOException;

@Listeners(CustomListener.class)
public class ProfileTest extends BaseTest {

    ProfilePage profileObj;
    String pathSS = "src/main/resources/airbnb/ss/";

    @Test(priority = 4, groups = {"profile","smoke"})
    public void testProfile() throws IOException {

        //Class objects
        BarraPage barraObj = new BarraPage(getDriver());
        profileObj = new ProfilePage(getDriver());
        //Click on Profile
        barraObj.profileButtonClick();
        //Verify Profile page
        Assert.assertTrue(profileObj.verifyProfileText());
    }
/*
    @Test(priority = 5, groups = {"profile","smoke"})
    public void testHosting() throws IOException {
        //Scroll
        profileObj.scrollToText("Learn about hosting");
        profileObj.takeScreenShot(pathSS, "profile_1");
    }

    @Test(priority = 5, groups = "profile")
    public void testMoney() throws IOException {
        //Scroll to partial text
        profileObj.scrollToPartialText("money");
        //Screenshot of Profile bottom page
        profileObj.takeScreenShot(pathSS, "profile_2");
        //Assert.assertTrue(false);
    }

 */
}