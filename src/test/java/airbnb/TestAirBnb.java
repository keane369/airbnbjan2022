package airbnb;

import core.BasePage;
import core.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.airbnb.BarraPage;
import pages.airbnb.ExplorePage;
import pages.airbnb.LoginPage;
import pages.airbnb.ProfilePage;

import java.io.IOException;

public class TestAirBnb extends BaseTest {

    @Test(priority = 6, groups = "template")
    public void testLogin() throws InterruptedException, IOException {

        //Class objects
        LoginPage loginObj = new LoginPage(getDriver());
        ExplorePage exploreObj = new ExplorePage(getDriver());
        BarraPage barraObj = new BarraPage(getDriver());
        ProfilePage profileObj = new ProfilePage(getDriver());
        String pathSS = "src/main/resources/airbnb/ss/";

        //Code

        /*
        //Continue with email
        loginObj.clickContinueWithEmail();

        //Enter credentials
        loginObj.completeEditTextEmail("ivan.rivas@cooltesters.com");
        loginObj.clickContinue();
        Thread.sleep(6000);

        loginObj.sendPassword("Appium123");
        loginObj.clickContinue();

        //Captcha
        Thread.sleep(50000);
         */

        
        //Verify explore page
        //Assert.assertTrue(exploreObj.isExplorePagePresent(),"Explore page is NOT displayed");

        //Look for destination Playa using a JSON file
        exploreObj.lookForDestination(exploreObj.getValueJSON("src/main/resources/airbnb/destination.json","Playa"));
        exploreObj.clickBackArrow();

        //Look for destination Catarata using excel file
        exploreObj.lookForDestination(exploreObj.getValueFromExcel("src/main/resources/airbnb/destinations.xlsx","Cataratas"));
        exploreObj.clickBackArrow();

        //Find a Restaurant using JSON file
        exploreObj.lookForDestination(exploreObj.getValueJSON("src/main/resources/airbnb/destination.json","Restaurante"));
        exploreObj.clickBackArrow();

        //Select a value from result search based on a text (Cancún)
        exploreObj.lookForDestination(exploreObj.getValueJSON("src/main/resources/airbnb/destination.json","Playa"));
        exploreObj.findDestinationClick("Cancun, Cancún");
        exploreObj.clickBackArrow();
        exploreObj.clickBackArrow();

        //Click on Profile
        barraObj.profileButtonClick();

        //Verify Profile page
        Assert.assertTrue(profileObj.verifyProfileText());

        //Swipe to get bottom of the screen
        //profileObj.swipeToElement(3, BasePage.Direction.UP);

        //Scroll
        profileObj.scrollToText("Learn about hosting");

        profileObj.takeScreenShot(pathSS,"profile_1");

        //Scroll to partial text
        profileObj.scrollToPartialText("money");

        //Validate Legal text on Profile page
        //Assert.assertTrue(profileObj.verifyLegalText(), "Legal text is not present");

        //Screenshot of Profile bottom page
        profileObj.takeScreenShot(pathSS,"profile_2");
    }
}
