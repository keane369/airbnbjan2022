package StepsDefinitions;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import pages.airbnb.BarraPage;
import pages.airbnb.ExplorePage;
import pages.airbnb.ProfilePage;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class MyStepDefs {

    AndroidDriver<AndroidElement> driver;
    ExplorePage exploreObj;

    @Given("Launch the app")
    public void launch_the_app() throws MalformedURLException {
        //Desired Capabilities
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "11");
        capabilities.setCapability("appActivity", "com.airbnb.android.feat.homescreen.HomeActivity");
        capabilities.setCapability("appPackage", "com.airbnb.android");
        //capabilities.setCapability("app", "/Users/ivanrivas/AirbnbMaven/src/main/resources/Selendroid/selendroid-test-app.apk");

        System.out.println("Creando el driver...");
        driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
        System.out.println("Driver creado!");

        //Implicit wait
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        System.out.println("Implicit wait set");
    }

    @When("User clicks on profile button")
    public void user_clicks_on_profile_button() {
        BarraPage barraObj = new BarraPage(driver);
        barraObj.profileButtonClick();
    }

    @Then("Profile screen should be visible")
    public void profile_screen_should_be_visible() {
        ProfilePage profileObj = new ProfilePage(driver);
        Assert.assertTrue(profileObj.verifyProfileText());
    }

    @When("I click on where are you going button")
    public void i_click_on_where_are_you_going_button(){
        exploreObj = new ExplorePage(driver);
        exploreObj.whereAreYouGoingClick();
    }

    @Then("I write Cancun")
    public void i_write_cancun(){
        exploreObj.enterDestination(exploreObj.getValueJSON("src/main/resources/airbnb/destination.json","Playa"));
    }

    @And("I hide keyboard")
    public void i_hide_keyboard(){
        exploreObj.tapBack();
    }

    @And("I click on back arrow")
    public void i_click_on_back_arrow(){
        exploreObj.tapBack();
    }
}
